This is a fork of the TFTPD64 application by Philippe Jounin, available from 
http://www.tftpd64.com/ or https://bitbucket.org/phjounin/tftpd64.

This fork adds the ability to build the TFTP server as a DLL, allowing a user's
application to run a TFTP server as part of the application.  TFTPD64 does not
currently allow this: it only currently allows either standalone execution as
a separate process, where support for command-line options is severely limited; 
or installation as a service, which requires the user to have administrator
rights.  Neither was considered suitable.

The DLL allows: full configuration of the TFTP server using the same options 
that would be available in the TFTPD64 application's INI file; execution of the
TFTP server from the user's application; full shutdown of the TFTP server when
the user's application shuts down (which is not guaranteed if running the TFTP
server as a separate process); no UI dialogs or messages; and execution from a
command-line application.

This fork is a Derivative Work of the original TFTPD64 application, and
therefore is released publicly to comply with the license.  The license does not
require the Executable Code (the compiled DLL) to be made available, so
building the DLL is left up to users.  Microsoft Visual Studio 2013 Express for
Desktop is recommended, and the supplied solution/project files work with that
compiler.

Note that applications which use this DLL are not themselves Derivative Works,
and need not be released under a compatible open-source license.
