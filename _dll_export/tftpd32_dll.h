//////////////////////////////////////////////////////
//
// Projet TFTPD32.  23 May 2017, G Bartlett, Elektron Technology (UK) plc
// 
// source released under artistic license (see license.txt)
//
//////////////////////////////////////////////////////

#ifndef TFTP32_DLL_H
#define TFTP32_DLL_H

#ifdef __cplusplus
extern "C" {
#endif

/** Dictionary element for DLL to set options directly when starting service.
*/
typedef struct
{
    char *key;
    char *value;
} tTftpd32Option;

// Function exports
extern int ServiceStart(tTftpd32Option* options, int numOptions);
extern int ServiceStop(void);

// Typedefs for pointers to functions
typedef int (*ServiceStartPtr)(tTftpd32Option* options, int numOptions);
typedef int (*ServiceStopPtr)(void);

#ifdef __cplusplus
}
#endif

#endif
