//////////////////////////////////////////////////////
//
// Projet TFTPD32.   Mai 98 Ph.jounin
// File DLL main.c:  The MAIN program for the DLL edition
//
// derivative work from sdk_service.cpp 
//                 by Craig Link - Microsoft Developer Support
// 
//
// source released under European Union Public License
//
//////////////////////////////////////////////////////


#include "headers.h"
#include <stdio.h>
#include "DLL stuff.h"


// A few global variables
char      szTftpd32IniFile [MAX_PATH];  // Full Path for INI file



// internal variables

BOOL                    bDebug = FALSE;

__declspec(dllexport) int ServiceStart(tTftpd32Option* options, int numOptions)
{
int Rc;
WSADATA WSAData;
  // ------------------------------------------
  // Start the App
  // ------------------------------------------
     	 
     Rc = WSAStartup (MAKEWORD(2,0), & WSAData);
     if (Rc != 0)
     {
         if (GetLastError() == WSAVERNOTSUPPORTED)
         {
             LogToMonitor("Error: Tftpd32 now requires winsock version 2\n");
         }
         else
         {
             LogToMonitor("Error: Can't init Winsocket\n");
         }
         return -1;
     }
	 else
	 {
 		 // start Services
         LogToMonitor("Tftpd32 starting from DLL\n");
         return StartTftpd32ServicesDll(options, numOptions);
	 } 
} /* ServiceStart */


__declspec(dllexport) int ServiceStop(void)
{
	int result = StopTftpd32Services ();
	Sleep (1500);
	WSACleanup ();
	LogToMonitor ("Tftpd32 DLL edition has ended\n");
	return result;
} // ServiceStop
