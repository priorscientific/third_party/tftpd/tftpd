//////////////////////////////////////////////////////
//
// Projet TFTPD32.   April 2007 Ph.jounin
// File service stuff.c:  services procedures
//
// derivative work from sdk_service.cpp 
//                 by Craig Link - Microsoft Developer Support
// 
// source released under artistic license (see license.txt)
//
//////////////////////////////////////////////////////

#ifndef DLL_STUFF_H
#define DLL_STUFF_H

#include "_common/settings.h"


#define SZSERVICEDISPLAYNAME  "TFTP server"
#define SZSERVICENAME		  "Tftpd32_svc_dll"
#define SZDEPENDENCIES        NULL
#define SZSERVDESCRIPTION     "TFTP server"

#define _tprintf printf
#define _stprintf sprintf

// internal variables
extern BOOL                    bDebug ;

// internal function prototypes
__declspec(dllexport) int ServiceStart(tTftpd32Option* options, int numOptions);
__declspec(dllexport) int ServiceStop(void);

int StartTftpd32ServicesDll(tTftpd32Option* options, int numOptions);

#endif