//////////////////////////////////////////////////////
//
// Projet TFTPD32.   Feb 99 By  Ph.jounin
// File start_threads.c:  Thread management
//
// The main function of the service
//
// source released under European Union Public License
//
//////////////////////////////////////////////////////


#include "headers.h"
#include <process.h>
#include "threading.h"
#include "bootpd_functions.h"

int StartTftpd32ServicesDll(tTftpd32Option* options, int numOptions)
{
    BOOL isOK;
    int result = -1;

    // Read settings
    isOK = Tftpd32PassSettings(options, numOptions);

    if (isOK)
    {
        //	DHCPReadConfig ();

        // starts worker threads
        result = StartMultiWorkerThreads(FALSE);
        LogToMonitor("Worker threads started\n");
    }

    return result;
} // StartTftpd32Services


int StopTftpd32Services (void)
{
   return TerminateWorkerThreads (FALSE);
}